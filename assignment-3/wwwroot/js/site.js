﻿new Vue({
    el: '#Calculator',
    data: {
        current: 0,
        log: []
    },
    methods: {
        Expresion: function (e) {
        	
            if (Number.isInteger(this.current)){
                this.current = '';
            }
                
            this.current += e;
           
        },
        result: function () {
            var logs = this.current;
            this.current = eval(this.current);
            this.log.push(logs + ("=" + this.current + ", "));

        },
        clear: function () {
        	
            this.current = 0;
            this.log='';
        },
        ce: function () {
            this.current = this.current.slice(0, -1);

        }
    }
});