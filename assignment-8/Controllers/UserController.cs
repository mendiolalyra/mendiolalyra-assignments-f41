﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using assignment_8.Models;

namespace assignment_8.Controllers
{
    public class UserController : Controller
    {
        // GET: User
        [HttpGet]
        public ActionResult Register(int id = 0)
        {
            account acc = new account();

            return View(acc);
        }
        [HttpGet]
        public ActionResult Login() {
            
            return View();
        }

        public ActionResult Welcome()
        {
            return View();
        }

            public ActionResult Login(account accountModel)
        {
            using (AccountModels account = new AccountModels())
            {

                if (account.accounts.Any(x => x.userName == accountModel.userName))
                {
                    if (account.accounts.Any(x => x.password == accountModel.password))
                    {
                        ViewBag.Message = "Welcome";
                        return View("Welcome", accountModel);
                    }
                    else {
                        ViewBag.Message = "Wrong username or password";
                        return View("Login", accountModel);
                    }

                }
                else {

                    ViewBag.Message = "Wrong username or password";
                    return View("Login", accountModel);
                }
               
            }

        }

        [HttpPost]
        public ActionResult Register(account accountModel) {
            using (AccountModels account = new AccountModels()) {

                if (account.accounts.Any(x => x.userName == accountModel.userName)) {

                    ViewBag.DuplicateMessage = "Username is already taken.";
                    return View("Register", accountModel);
                }
                account.accounts.Add(accountModel);
                account.SaveChanges();
            }
            ModelState.Clear();
            ViewBag.SuccessMessage = "You are now registered.";
            return View("Register", new account());
        }
    }
}