

namespace assignment_8.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;

    public partial class account
    {
        public int id { get; set; }
        [Required(ErrorMessage ="This Field is Required")]
        public string userName { get; set; }

        [Required(ErrorMessage = "This Field is Required")]
        [DataType(DataType.Password)]
        public string password { get; set; }

        [Required(ErrorMessage = "This Field is Required")]
        [DataType(DataType.Password)]
        [DisplayName ("Confirm Password") ]
        [Compare("password")]
        public string confirm { get; set; }

        [Required(ErrorMessage = "This Field is Required")]
        public string firstName { get; set; }

        [Required(ErrorMessage = "This Field is Required")]
        public string middleName { get; set; }

        [Required(ErrorMessage = "This Field is Required")]
        public string lastName { get; set; }

        [Required(ErrorMessage = "This Field is Required")]
        public int Age { get; set; }

        [Required(ErrorMessage = "This Field is Required")]
        public string Address { get; set; }

        [Required(ErrorMessage = "This Field is Required")]
        public string sex { get; set; }
    }
}
